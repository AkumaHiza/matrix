#include <iostream>

#include "matrix.hpp"

int main() {
  std::vector<std::vector<float>> v1{
      {1, 2, 3, 4}, {5, 6, 7, 8}, {9, 0, 1, 2}, {3, 4, 5, 6}, {7, 8, 9, 0}};
  std::vector<std::vector<float>> v2{
      {8, 7, 6, 5}, {4, 3, 2, 1}, {0, 9, 8, 7}, {6, 5, 4, 3}, {2, 1, 0, 9}};
  Matrix m1(v2);
  Matrix m2(v2);
  m1.add(m2);
  m1.show();

  return 0;
}