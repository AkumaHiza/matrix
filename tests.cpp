#include "matrix.hpp"
#include <gtest/gtest.h>

class AddTest : public ::testing::Test
{
protected:
  std::vector<std::vector<float>> sequence{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 0, 1, 2}, {3, 4, 5, 6}, {7, 8, 9, 0}};
  std::vector<std::vector<float>> sequence_reverse{{8, 7, 6, 5}, {4, 3, 2, 1}, {0, 9, 8, 7}, {6, 5, 4, 3}, {2, 1, 0, 9}};
  std::vector<std::vector<float>> ones{{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}};
  std::vector<std::vector<float>> ones_shorter{{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}};
  std::vector<std::vector<float>> result{{2, 3, 4, 5}, {6, 7, 8, 9}, {10, 1, 2, 3}, {4, 5, 6, 7}, {8, 9, 10, 1}};
};
class SubtractTest : public ::testing::Test
{
protected:
  std::vector<std::vector<float>> sequence{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 0, 1, 2}, {3, 4, 5, 6}, {7, 8, 9, 0}};
  std::vector<std::vector<float>> sequence_reverse{{8, 7, 6, 5}, {4, 3, 2, 1}, {0, 9, 8, 7}, {6, 5, 4, 3}, {2, 1, 0, 9}};
  std::vector<std::vector<float>> ones{{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}};
  std::vector<std::vector<float>> ones_shorter{{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}};
  std::vector<std::vector<float>> result{{0, 1, 2, 3}, {4, 5, 6, 7}, {8, -1, 0, 1}, {2, 3, 4, 5}, {6, 7, 8, -1}};
};
class MultipleTest : public ::testing::Test
{
protected:
  std::vector<std::vector<float>> sequence{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 1, 2, 3}};
  std::vector<std::vector<float>> sequence_reverse{{9, 8}, {7, 6}, {5, 4}, {3, 2}};
  std::vector<std::vector<float>> result{{50, 40}, {146, 120}, {107, 92}};
  std::vector<std::vector<float>> square{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 1, 2, 3}, {4, 5, 6, 7}};
  std::vector<std::vector<float>> square_reverse{{9, 8, 7, 6}, {5, 4, 3, 2}, {1, 9, 8, 7}, {6, 5, 4, 3}};
};
class TransposeTest : public ::testing::Test
{
protected:
  std::vector<std::vector<float>> sequence{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 1, 2, 3}};
  std::vector<std::vector<float>> sequence_transposed{{1, 5, 9}, {2, 6, 1}, {3, 7, 2}, {4, 8, 3}};
};
class OperatorTest : public ::testing::Test
{
protected:
  std::vector<std::vector<float>> sequence{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 0, 1, 2}, {3, 4, 5, 6}, {7, 8, 9, 0}};
  std::vector<std::vector<float>> sequence_reverse{{8, 7, 6, 5}, {4, 3, 2, 1}, {0, 9, 8, 7}, {6, 5, 4, 3}, {2, 1, 0, 9}};
};

////////////// ADDING

TEST_F(AddTest, AddingReturn)
{
  Matrix s(sequence);
  Matrix o(ones);

  EXPECT_TRUE(s.add(o));
}

TEST_F(AddTest, AddingResult)
{
  Matrix s(sequence);
  Matrix o(ones);
  Matrix r(result);
  s.add(o);

  EXPECT_TRUE(s == r);
}

TEST_F(AddTest, AddingIncorrect)
{
  Matrix s(sequence);
  Matrix os(ones_shorter);

  EXPECT_FALSE(s.add(os));
}

TEST_F(AddTest, CommutativeAdding)
{

  Matrix s(sequence);
  Matrix sr(sequence_reverse);
  Matrix sr2(sequence_reverse);
  sr.add(s);
  s.add(sr2);

  EXPECT_TRUE(sr == s);
}

////////////// == OPERATOR

TEST_F(OperatorTest, CompareOperatorTrue)
{
  Matrix s(sequence);

  EXPECT_TRUE(s == s);
}

TEST_F(OperatorTest, ComapareOperatorFalse)
{
  Matrix s(sequence);
  Matrix sr(sequence_reverse);

  EXPECT_FALSE(sr == s);
}

////////////// SUBTRACT

TEST_F(SubtractTest, SubtractReturn)
{
  Matrix s(sequence);
  Matrix o(ones);

  EXPECT_TRUE(s.subtract(o));
}

TEST_F(SubtractTest, SubtractResult)
{
  Matrix s(sequence);
  Matrix o(ones);
  Matrix r(result);
  s.subtract(o);

  EXPECT_TRUE(s == r);
}

TEST_F(SubtractTest, SubtractIncorrect)
{
  Matrix s(sequence);
  Matrix os(ones_shorter);

  EXPECT_FALSE(s.subtract(os));
}

TEST_F(SubtractTest, CommutativeSubtract)
{

  Matrix s(sequence);
  Matrix sr(sequence_reverse);
  Matrix sr2(sequence_reverse);
  sr.subtract(s);
  s.subtract(sr2);

  EXPECT_FALSE(sr == s);
}

////////////// MULTIPLY

TEST_F(MultipleTest, MultiplyReturn)
{
  Matrix s(sequence);
  Matrix sr(sequence_reverse);

  EXPECT_TRUE(s.multiply(sr));
}

TEST_F(MultipleTest, MultiplyResult)
{

  Matrix s(sequence);
  Matrix sr(sequence_reverse);
  Matrix r(result);
  s.multiply(sr);

  EXPECT_TRUE(s == r);
}

TEST_F(MultipleTest, MultiplySelfFalse)
{
  Matrix s(sequence);

  EXPECT_FALSE(s.multiply(s));
}

TEST_F(MultipleTest, MultiplySelfTrue)
{
  Matrix s(square);

  EXPECT_TRUE(s.multiply(s));
}

TEST_F(MultipleTest, CommutativeMultiply)
{
  Matrix s(square);
  Matrix sr(square_reverse);
  Matrix sr2(square_reverse);

  sr.multiply(s);
  s.multiply(sr2);

  EXPECT_FALSE(sr == s);
}

////////////// TRANSPOSE

TEST_F(TransposeTest, TransposeResult)
{
  Matrix s(sequence);
  Matrix st(sequence_transposed);
  s.transpose();

  EXPECT_TRUE(s == st);
}

TEST_F(TransposeTest, TransposeSelf)
{
  Matrix s(sequence);
  Matrix s2(sequence);
  s.transpose();
  s.transpose();

  EXPECT_TRUE(s == s2);
}
