#pragma once
#include <iostream>
#include <vector>

class Matrix {
 public:
  Matrix(std::vector<std::vector<float>> v);
  bool operator==(Matrix const &m);
  void show();
  bool add(Matrix m);
  bool subtract(Matrix m);
  bool multiply(Matrix m);
  void transpose();

 private:
  std::vector<std::vector<float>> matrix;
};