#include "matrix.hpp"

Matrix::Matrix(std::vector<std::vector<float>> vec) { matrix = vec; }

bool Matrix::operator==(Matrix const &s) {
  int first_rows = matrix.size();
  int first_columns = matrix[0].size();
  int second_rows = s.matrix.size();
  int second_columns = s.matrix[0].size();
  if (first_rows != second_rows) return false;
  if (first_columns != second_columns) return false;
  for (int r = 0; r < first_rows; r++) {
    for (int c = 0; c < first_columns; c++) {
      if (matrix[r][c] != s.matrix[r][c]) return false;
    }
  }
  return true;
}

void Matrix::show() {
  int rows = matrix.size();
  int columns = matrix[0].size();

  for (int r = 0; r < rows; r++) {
    for (int c = 0; c < columns; c++) {
      std::cout << matrix[r][c] << " ";
    }
    std::cout << "\n";
  }
  std::cout << "\n";
}

bool Matrix::add(Matrix s) {
  int rows = matrix.size();
  int columns = matrix[0].size();

  if ((columns != s.matrix[0].size()) || (rows != s.matrix.size())) {
    std::cerr << "Adding failed. Matrices must have the same dimensions.\n";
    return false;
  }
  for (int r = 0; r < rows; r++) {
    for (int c = 0; c < columns; c++) {
      matrix[r][c] += s.matrix[r][c];
    }
  }
  return true;
}

bool Matrix::subtract(Matrix s) {
  int rows = matrix.size();
  int columns = matrix[0].size();

  if ((columns != s.matrix[0].size()) || (rows != s.matrix.size())) {
    std::cerr
        << "Subtracting failed. Matrices must have the same dimensions.\n";
    return false;
  }
  for (int r = 0; r < rows; r++) {
    for (int c = 0; c < columns; c++) {
      matrix[r][c] -= s.matrix[r][c];
    }
  }
  return true;
}

bool Matrix::multiply(Matrix s) {
  int first_rows = matrix.size();
  int first_columns = matrix[0].size();
  int second_rows = s.matrix.size();
  int second_columns = s.matrix[0].size();

  if (first_columns != second_rows) {
    std::cerr << "Multiplication failed. Number of columns in the first matrix "
                 "must be equal to the number of rows in the second matrix.\n";
    return false;
  }
  std::vector<std::vector<float>> new_vector;
  new_vector.resize(first_rows, std::vector<float>(second_columns));
  float sum = 0.0;
  for (int first = 0; first < first_rows; first++) {
    for (int second = 0; second < second_columns; second++) {
      for (int elem = 0; elem < first_columns; elem++) {
        sum += matrix[first][elem] * s.matrix[elem][second];
      }
      new_vector[first][second] = sum;
      sum = 0.0;
    }
  }
  matrix = new_vector;
  return true;
}

void Matrix::transpose() {
  int rows = matrix.size();
  int columns = matrix[0].size();

  std::vector<std::vector<float>> new_vector;
  new_vector.resize(columns, std::vector<float>(rows));
  for (int c = 0; c < columns; c++) {
    for (int r = 0; r < rows; r++) {
      new_vector[c][r] = matrix[r][c];
    }
  }
  matrix = new_vector;
}